﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Entity
{
    public class TypeDocument : EntityBase
    {
        public string TypeDocumentCode { get; set; }
        public string TypeDocumentName { get; set; }
    }
}
