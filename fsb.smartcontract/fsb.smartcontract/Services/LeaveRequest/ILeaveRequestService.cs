﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Model.LeaveRequest;
using Model.Requests;
using Model.Response;
using System.Collections.Generic;

namespace fsb.smartcontract.Services.LeaveRequest
{
    public interface ILeaveRequestService
    {
        TableResponse<LeaveRequestViewModel> GetListLeaveRequest(SearchLeaveRequestModel search);
        List<SelectListItem> GetListClassForCombo();
        Response<string> CreateLeaveRequest(LeaveRequestModel model);
        Response<LeaveRequestModel> GetLeaveRequestById(LeaveRequestModel model);
        Response<string> UpdateStatusLeaveRequest(LeaveRequestModel model);
        Response<string> UpdateLeaveRequest(LeaveRequestModel model);
        Response<string> DeleteLeaveRequest(LeaveRequestModel model);
    }
}
