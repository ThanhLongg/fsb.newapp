﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Model.LeaveRequest;
using Model.Requests;
using Model.Response;
using Repository.LeaveRequest;

namespace fsb.smartcontractapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LeaveRequestController : BaseController
    {
        private readonly ILeaveRequestRepository _leaveRequestRepository;

        public LeaveRequestController(
            ILeaveRequestRepository leaveRequestRepository,
            ILogger<LeaveRequestController> logger,
            IConfiguration configuration
            ) : base(configuration, logger)
        {
            _leaveRequestRepository = leaveRequestRepository;
        }

        [Route("GetListLeaveRequest")]
        [HttpPost]
        public TableResponse<LeaveRequestViewModel> GetListLeaveRequest(SearchLeaveRequestModel search)
        {
            return _leaveRequestRepository.GetListLeaveRequest(search);
        }

        [Route("CreateLeaveRequest")]
        [HttpPost]
        public Response<string> CreateLeaveRequest(LeaveRequestModel model)
        {
            return _leaveRequestRepository.CreateLeaveRequest(model);
        }

        [Route("GetLeaveRequestById")]
        [HttpPost]
        public Response<LeaveRequestModel> GetLeaveRequestById(LeaveRequestModel model)
        {
            return _leaveRequestRepository.GetLeaveRequestById(model);
        }

        [Route("UpdateStatusLeaveRequest")]
        [HttpPost]
        public Response<string> UpdateStatusLeaveRequest(LeaveRequestModel model)
        {
            return _leaveRequestRepository.UpdateStatusLeaveRequest(model);
        }

        [Route("UpdateLeaveRequest")]
        [HttpPost]
        public Response<string> UpdateLeaveRequest(LeaveRequestModel model)
        {
            return _leaveRequestRepository.UpdateLeaveRequest(model);
        }

        [Route("DeleteLeaveRequest")]
        [HttpPost]
        public Response<string> DeleteLeaveRequest(LeaveRequestModel model)
        {
            return _leaveRequestRepository.DeleteLeaveRequest(model);
        }
    }
}
