﻿using Common;
using Entity.Context;
using Entity.Entity;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Model.Response;
using Model.UserInfo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Repository.UserInfo
{
    public class UserInfoRepository : Repository<UserInfoRepository>, IUserInfoRepository
    {
        private readonly IConfiguration _configuration;
        public UserInfoRepository(
            ILogger<UserInfoRepository> logger,
            IConfiguration configuration,
            FsbSmartContractContext context
            ) : base(context, logger)
        {
            _configuration = configuration;
        }

        public Response<UserViewModel> GetUserByUserName(UserViewModel model)
        {
            Response<UserViewModel> res = new Response<UserViewModel>();
            try
            {
                var user = _context.Users.Where(x => x.UserName == model.UserName && x.IsDelete == false).FirstOrDefault();
                if (user == null)
                {
                    res.Code = StatusCodes.Status404NotFound;
                    res.Message = "Không tìm thấy người dùng!";
                    return res;
                }
                else
                {
                    //var userRole = _context.UserRoles.FirstOrDefault(x => x.UserId == user.Id);
                    //var userRoleName = "";
                    //var userRoleNormalizedName = "";
                    //if (userRole != null)
                    //{
                    //    var dataUserRole = _context.Roles.FirstOrDefault(x => x.Id == userRole.RoleId);
                    //    userRoleName = dataUserRole.Name;
                    //    userRoleNormalizedName = dataUserRole.NormalizedName;
                    //}

                    //var userProject = _context.UserProjects.FirstOrDefault(x => x.UserId == user.Id);

                    res.Code = StatusCodes.Status200OK;
                    res.Data = new UserViewModel()
                    {
                        Id = user.Id,
                        UserName = user.UserName,
                        FullName = user.FullName,
                        //UserRoleName = userRoleName,
                        //UserRoleNormalizedName = userRoleNormalizedName,
                        IsAdmin = user.IsAdmin,
                        Password = user.PasswordHash
                    };
                    return res;
                }

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Code = StatusCodes.Status400BadRequest;
                res.Message = "Xảy ra lỗi khi lấy thông tin người dùng!";
                return res;
            }
        }
    }
}
