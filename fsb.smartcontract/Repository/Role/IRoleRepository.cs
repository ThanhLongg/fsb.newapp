﻿using Model.Requests;
using Model.Response;
using Model.Role;
using Model.UserInfo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Role
{
    public interface IRoleRepository
    {
        Response<UserViewModel> GetRoleByUserId(Guid userId);
        TableResponse<RoleViewModel> GetListRole(SearchRoleModel search);
        Response<string> CreateRole(RoleModel model);
        Response<string> DeleteRole(RoleModel model);
        Response<RoleModel> GetRoleById(RoleModel model);
        Response<string> UpdateRole(RoleModel model);
    }
}
